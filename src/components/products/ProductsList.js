import React, {PropTypes} from 'react';
import ProductListItem from './ProductListItem';

const ProductsList = ({products}) => {
  return (
    <ul className="products-container">{
      products.map((product) => {
        return (<ProductListItem key={product._id} product={product}/>);
      })
    }
    </ul>
  );
};
ProductsList.propTypes = {
  products: PropTypes.array.isRequired
};

export default ProductsList;
