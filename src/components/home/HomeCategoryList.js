import React, {PropTypes} from 'react';
import HomeCategoryItem from './HomeCategoryItem';

const HomeCategoryList = ({homeCategorys}) => {
	return (
		<ul className="home-container">{
			homeCategorys.map((category) => {
				return (<HomeCategoryItem key={category.link} category={category}/>);
			})
		}
		</ul>
	);
};
HomeCategoryList.propTypes = {
	homeCategorys: PropTypes.array.isRequired
};

export default HomeCategoryList;
