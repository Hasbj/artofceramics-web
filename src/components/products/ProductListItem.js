import React, {PropTypes} from 'react';
import {Link} from 'react-router';

const ProductListItem = ({product}) => {
  return (
    <li className="product-item">
      <Link to={'/product/'+product._id}>
        <img src={product.imgSrc} />
        <div className="product-item-name">{product.name}</div>
        <div className="product-item-price">{product.price}</div>
      </Link>
    </li>
  );
};

ProductListItem.propTypes = {
  product: PropTypes.object.isRequired
};

export default ProductListItem;
