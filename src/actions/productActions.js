import ProductApi from '../api/productApi';
import * as types from './actionTypes';

export function loadProductsSuccess(products){
  return {type: types.LOAD_PRODUCTS_SUCCESS, products};
}

export function loadProducts(){
  return dispatch =>{
    return ProductApi.getProducts().then(products =>{
      dispatch(loadProductsSuccess(products));
    }).catch(error => {
      throw(error);
    });
  };
}
