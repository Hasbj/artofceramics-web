import React from 'react';
import {Route, IndexRoute} from 'react-router';
import App from './components/App';
import HomePage from './components/home/HomePage';
import ProductsPage from './components/products/ProductsPage';
import ProductDetail from './components/products/ProductDetailManager';
import ContactsPage from './components/contacts/ContactsPage';
import AboutPage from './components/about/AboutPage';

export default(
  <Route path="/" component={App}>
    <IndexRoute component={HomePage}/>
    <Route path="about" component={AboutPage}/>
    <Route path="products" component={ProductsPage}/>
    <Route path="product/:id" component={ProductDetail}/>
    <Route path="contacts" component={ContactsPage}/>
  </Route>
);
