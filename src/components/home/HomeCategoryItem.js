import React, {PropTypes} from 'react';
import {Link} from 'react-router';

const HomeCategoryItem = ({category}) => {
	if(category.isAdvert) return (
		<li className="aoc-home-advertisement">
      <Link to={category.link}>
        {category.title}
      </Link>
		</li>
	);

	return (
		<li className={"aoc-home-item " + category.className}>
      <Link to={'/products' + (category.category ? '/' + category.category : '' )}>
        <img
          src={category.src}/>
        <div className="aoc-home-category-name">{category.title}</div>
      </Link>
		</li>
	);
};

HomeCategoryItem.propTypes = {
	category:PropTypes.object.isRequired
};

export default HomeCategoryItem;
