import React, {PropTypes} from 'react';
import DropDownFilter from '../common/DropDownFilter';
import '../../styles/components/products/product-filter.scss'

const ProductFilters = ({filters}) => {
  return (
    <div className="product-filter">
      {filters.map((filter)=>{
        return (<DropDownFilter filter={filter}/>);
      })}
    </div>
  );
};

ProductFilters.propTypes = {
  filters: PropTypes.array.isRequired
};

export default ProductFilters;
