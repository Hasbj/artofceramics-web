import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as productActions from '../../actions/productActions';
import ProductDetails from './ProductDetails';
import conf from '../../config/config';

class ProductDetailManager extends React.Component {

  constructor(props, context) {
    super(props, context);

    this.state = {
      product: Object.assign({}, this.props.product),
      order: {
        currentCount:1,
        countCanBeLess:false,
        countCanBeBiger: true
      },
      viewSetting: {
        viewSpec: false
      }
    };
    this.incrementOrderCount = this.incrementOrderCount.bind(this);
    this.decrementOrderCount = this.decrementOrderCount.bind(this);
    this.changeViewSpec = this.changeViewSpec.bind(this);
  }

  incrementOrderCount(){
    const order = this.state.order;
    if(order.currentCount < conf.maxOrderCount) {
      order.currentCount++;
      order.countCanBeBiger = order.currentCount < conf.maxOrderCount;
      order.countCanBeLess = order.currentCount > 1;
      this.setState({order:order});
    }
  }
  decrementOrderCount(){
    const order = this.state.order;
    if(order.currentCount > 1) {
      order.currentCount++;
      order.countCanBeBiger = order.currentCount < conf.maxOrderCount;
      order.countCanBeLess = order.currentCount > 1;
      this.setState({order:order});
    }
  }

  changeViewSpec(){
    const viewSetting = this.state.viewSetting;
    viewSetting.viewSpec = !viewSetting.viewSpec;
    this.setState({viewSetting:viewSetting});
  }

  render() {
    return (
      <ProductDetails
        product={this.state.product}
        order={this.state.order}
        viewSetting={this.state.viewSetting}
        changeViewSpec={this.changeViewSpec}
        incrementOrderCount={this.incrementOrderCount}
        decrementOrderCount={this.decrementOrderCount}
      />
    );
  }
}

ProductDetailManager.propTypes = {
  product: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired
};

ProductDetailManager.contextTypes = {
  router: PropTypes.objectOf
};

function getProductById(products,id){
  const product = products.filter(product => product._id == id);
  if(product) return product[0];
  return null;
}

function mapStateToProps(state, ownProps) {
  let productId = ownProps.params.id;

  let product = {id:'', imgSrc:'', name:'', price:'0'};

  if(productId && state.products.length > 0){
    product = getProductById(state.products,productId);
  }


  return {
    product: product
  };
}

function mapDispatchToProps(dispatch) {
  return {actions: bindActionCreators(productActions, dispatch)};
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetailManager);
