import React, {PropTypes} from 'react';
import AddToCart from '../common/AddToCart';
import '../../styles/components/products/product_details.scss'

const ProductDetails = ({product, order, viewSetting, incrementOrderCount, decrementOrderCount, changeViewSpec}) => {
  return (
    <div className="aoc-product-details-container">
      <div className="product-details-img-area">
        <img className="product-details-img" src={product.imgLgSrc}/>
        <img className={"product-details-spec-img" + (viewSetting.viewSpec ? '' : ' block')} src={product.imgDetailsSrc}/>
      </div>
      <div className="product-details-main-info">
        <h2 className="product-details-main-info-name">
          {product.name}
        </h2>
        <h3 className="product-details-main-info-price">
          {product.price}
        </h3>
      </div>
      <div className="product-details-info">
        {product.attr}
        <span className="product-details-show-spec" onClick={changeViewSpec}>
          {
            (!viewSetting.viewSpec ? 'Показать информацию о продукте' : 'Скрыть информацию о продукте')
          }
        </span>
      </div>
      <div className="product-details-desc">
        {product.desc}
      </div>
      <AddToCart currentCount={order.currentCount}
        incrementOrderCount = {incrementOrderCount}
        decrementOrderCount = {decrementOrderCount}
        countCanBeLess = {order.countCanBeLess}
        countCanBeBiger = {order.countCanBeBiger}
      />
      <img className="product-details-img-additional" src={product.aditionalImgsrc} />
    </div>
  );
};

ProductDetails.propTypes = {
  product:PropTypes.object.isRequired,
  order: PropTypes.object.isRequired,
  viewSetting: PropTypes.object.isRequired,
  changeViewSpec: PropTypes.func.changeViewSpec,
  incrementOrderCount: PropTypes.func.isRequired,
  decrementOrderCount: PropTypes.func.isRequired
};

export default ProductDetails;
