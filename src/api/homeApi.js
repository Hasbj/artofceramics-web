import conf from '../config/config';
class HomeApi {
	static getHomeCategorys() {
		return fetch(conf.restApiUri + `/home-category`)
			.then(response => response.json());
	}
}

export default HomeApi;
