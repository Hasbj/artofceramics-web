import React, {PropTypes} from 'react';
import '../../styles/components/common/drop-down-filter.scss';

const DropDownFilter = ({filter}) => {
  return (
    <div className="dropdown-filter open">
      <div className="filterName">{'Sort by ' + filter.name}</div>
      <div className="close"><i className="fa fa-times" aria-hidden="true"></i></div>
      <ul className="dropdown-list">
        {filter.options.map((item)=>{
          return (<li>{item.name}</li>);
        })}
      </ul>
    </div>
  );
};

DropDownFilter.propTypes = {
  filter: PropTypes.object.isRequired
};

export default DropDownFilter;
