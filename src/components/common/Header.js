import React, {PropTypes} from 'react';
import {Link, IndexLink} from 'react-router';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as headerActions from '../../actions/headerActions';
import '../../styles/components/common/header.scss';

class Header extends React.Component{
	render(){
		return (
			<header>
				<div className="row">
					<div className="aoc-logo col-12 col-md-6">
						<div className="aoc-logo-wrapp">
							<Link to={'/'}>
								ART<span>OF</span>CERAMICS
							</Link>
						</div>
					</div>
					<div className="aoc-nav-item aoc-menu col-6 col-md-3">
						<i className="fa fa-bars" aria-hidden="true"></i>
						<nav>
							<ul className="aoc-hidden-item">
								<li><IndexLink to="/" activeClassName="active">Главная</IndexLink></li>
								<li><Link to="/products" activeClassName="active">Продукция</Link></li>
								<li><Link to="/contacts" activeClassName="active">Контакты</Link></li>
								<li><Link to="/about" activeClassName="active">О Нас</Link></li>
							</ul>
						</nav>
					</div>
					<div className="aoc-nav-item aoc-cart col-6 col-md-3">
						<i className="fa fa-shopping-cart" aria-hidden="true"></i>
						<div className="aox-shopping-cart aoc-hidden-item">
							<span className="aox-shopping-cart-empty">Пока тут пусто</span>
						</div>
					</div>
				</div>
			</header>
		);
	}
}

Header.propTypes = {
	header: PropTypes.object.isRequired,
	actions: PropTypes.object.isRequired
};
function mapStateToProps(state,ownProps){
	return {
		header:state.header
	};
}
function mapDispatchToProps(dispatch){
	return {
		actions:bindActionCreators(headerActions,dispatch)
	};
}
export default connect(mapStateToProps,mapDispatchToProps)(Header);


