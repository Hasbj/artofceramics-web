import React,{PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as homeActions from '../../actions/homeActions';
//import '../../styles/components/home/home.scss';

class ContactsPage extends React.Component {

  constructor(props,context){
    super(props,context);
  }

  render() {
    const {categorys} = this.props;
    return (
      <div>
        <h1>Hello, Customers! IT is contact page</h1>
      </div>
    );
  }
}
ContactsPage.propTypes = {
  //categorys: PropTypes.array.isRequired,
  actions: PropTypes.object.isRequired
};
function mapStateToProps(state,ownProps){
  return {
    //categorys:state.home
  };
}
function mapDispatchToProps(dispatch){
  return {
    actions:bindActionCreators(homeActions,dispatch)
  };
}
export default connect(mapStateToProps,mapDispatchToProps)(ContactsPage);
