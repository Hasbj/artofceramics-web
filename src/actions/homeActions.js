import HomeApi from '../api/homeApi';
import * as types from './actionTypes';

export function loadHomeCategorysSuccess(categorys){
	return {type: types.LOAD_HOME_CATEGORYS_SUCCESS, categorys};
}

export function loadHomeCategorys(){
	return dispatch =>{
		return HomeApi.getHomeCategorys().then(categorys =>{
			dispatch(loadHomeCategorysSuccess(categorys));
		}).catch(error => {
			throw(error);
		});
	};
}
