import React,{PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as productActions from '../../actions/productActions';
import '../../styles/components/products/products.scss';
import ProductsList from './ProductsList';
import ProductFilters from './ProductFilters';

class ProductsPage extends React.Component {

  constructor(props,context){
    super(props,context);
  }

  render() {
    const {products} = this.props;
    const filters = [{name:'Type', options:[{name:'SomeType1', id: 'some_type_1'}, {name:'SomeType2', id: 'some_type_2'}, {name:'SomeType3', id: 'some_type_3'}]}
    ,{name:'Line', options:[{name:'SomeType1', id: 'some_type_1'}, {name:'SomeType2', id: 'some_type_2'}, {name:'SomeType3', id: 'some_type_3'}]}
    ,{name:'Color', options:[{name:'SomeType1', id: 'some_type_1'}, {name:'SomeType2', id: 'some_type_2'}, {name:'SomeType3', id: 'some_type_3'}]}];
    return (
      <div id="aoc-products-wrap">
        <img className="product-main-image" src="http://cdn.shopify.com/s/files/1/0702/6935/t/1/assets/collections_top_image.jpg?10701860855399185941"/>
        <ProductFilters filters={filters}/>
        <ProductsList products={products}/>
      </div>
    );
  }
}
ProductsPage.propTypes = {
  products: PropTypes.array.isRequired,
  actions: PropTypes.object.isRequired
};
function mapStateToProps(state,ownProps){
  return {
    products:state.products
  };
}
function mapDispatchToProps(dispatch){
  return {
    actions:bindActionCreators(productActions,dispatch)
  };
}
export default connect(mapStateToProps,mapDispatchToProps)(ProductsPage);
