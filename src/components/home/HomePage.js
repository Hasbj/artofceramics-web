import React,{PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as homeActions from '../../actions/homeActions';
import '../../styles/components/home/home.scss';
import CategoryList from './HomeCategoryList';

class HomePage extends React.Component {

	constructor(props,context){
		super(props,context);
	}

	render() {
		const {categorys} = this.props;
		return (
			<div>
				<CategoryList homeCategorys={categorys}/>
			</div>
		);
	}
}
HomePage.propTypes = {
	categorys: PropTypes.array.isRequired,
	actions: PropTypes.object.isRequired
};
function mapStateToProps(state,ownProps){
	return {
		categorys:state.home
	};
}
function mapDispatchToProps(dispatch){
	return {
		actions:bindActionCreators(homeActions,dispatch)
	};
}
export default connect(mapStateToProps,mapDispatchToProps)(HomePage);
