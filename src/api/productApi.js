import conf from '../config/config';
class ProductApi {
  static getProducts() {
    return fetch(conf.restApiUri + `/products`)
      .then(response => response.json());
  }
}

export default ProductApi;
