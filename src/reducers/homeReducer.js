import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function homeReducer (state = initialState.homeCategorys,action){
	switch(action.type){
		case types.LOAD_HOME_CATEGORYS_SUCCESS:
			return action.categorys;

		default:
			return state;
	}
}
