import React, {PropTypes} from 'react';
import '../../styles/components/common/add-to-cart.scss';

const AddToCart = ({currentCount, incrementOrderCount, decrementOrderCount, countCanBeBiger, countCanBeLess}) => {
  return (
    <div className="aoc-product-details-buttons-section">
      <div className="count-buttons">
        <button className={(countCanBeLess ? '' : ' block ') + "count-minus"} onClick={decrementOrderCount}>-</button>
        <div className="count">{currentCount}</div>
        <button className={(countCanBeBiger ? '' : ' block ') + "count-plus"} onClick={incrementOrderCount}>+</button>
      </div>
      <div className="add-to-cart">Добавить в корзину</div>
    </div>
  );
};

AddToCart.propTypes = {
  currentCount: PropTypes.number.isRequired,
  incrementOrderCount: PropTypes.func.isRequired,
  decrementOrderCount: PropTypes.func.isRequired,
  countCanBeBiger: PropTypes.bool,
  countCanBeLess: PropTypes.bool
};

export default AddToCart;
