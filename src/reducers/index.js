import {combineReducers} from 'redux';
import courses from './courseReducer';
import authors from './authorReducer';
import home from './homeReducer';
import products from './productReducer';
import ajaxCallsInProgress from './ajaxStatusReducer';

const rootReducer = combineReducers({
	courses,
	authors,
	home,
  products
});

export default rootReducer;
