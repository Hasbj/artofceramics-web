import React, {PropTypes} from 'react';
import {Link, IndexLink} from 'react-router';
import '../../styles/components/common/footer.scss';
import $ from 'jquery';

class Footer extends React.Component {
	render(){
    return (
      <footer>
        <nav>
          <ul>
            <li>
              <a href="/">Быть вкурсе</a>
            </li>
            <li>
              <a href="/">Cookies</a>
            </li>
          </ul>
        </nav>
        <div id="aoc-footer-up" className="aoc-footer-page-up" onClick={()=>{$('body,html').animate({scrollTop:0},700);}}>
          <i className="fa fa-long-arrow-up" aria-hidden="true"></i>
        </div>
        <nav>
          <ul>
            <li>
              <a href="/">Условия сотрудничества</a>
            </li>
            <li>
              <a href="/">Ближайшие мероприятия</a>
            </li>
          </ul>
        </nav>
      </footer>
    );
  }
}

Footer.propTypes = {

};

export default Footer;


